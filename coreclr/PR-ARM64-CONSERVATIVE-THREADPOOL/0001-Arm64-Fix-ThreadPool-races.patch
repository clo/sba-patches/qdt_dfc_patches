From c0ef151073cba6e7415dd0f46e01c984eeb30bdb Mon Sep 17 00:00:00 2001
From: Steve MacLean <sdmaclea.qdt@qualcommdatacenter.com>
Date: Wed, 11 Apr 2018 18:24:36 -0400
Subject: [PATCH] [Arm64] Fix ThreadPool races

---
 src/mscorlib/src/System/Threading/ThreadPool.cs | 63 ++++++++++---------------
 1 file changed, 26 insertions(+), 37 deletions(-)

diff --git a/src/mscorlib/src/System/Threading/ThreadPool.cs b/src/mscorlib/src/System/Threading/ThreadPool.cs
index ad70726..c22cbf9 100644
--- a/src/mscorlib/src/System/Threading/ThreadPool.cs
+++ b/src/mscorlib/src/System/Threading/ThreadPool.cs
@@ -273,48 +273,29 @@ namespace System.Threading
                         return null;
                     }
 
-                    // Decrement the tail using a fence to ensure subsequent read doesn't come before.
                     tail -= 1;
-                    Interlocked.Exchange(ref m_tailIndex, tail);
+                    m_tailIndex = tail;
 
-                    // If there is no interaction with a take, we can head down the fast path.
-                    if (m_headIndex <= tail)
-                    {
                     int idx = tail & m_mask;
-                        IThreadPoolWorkItem obj = Volatile.Read(ref m_array[idx]);
 
-                        // Check for nulls in the array.
-                        if (obj == null) continue;
+                    // We use Interlocked.Exchange to claim from m_array w/o locks
+                    IThreadPoolWorkItem obj = Interlocked.Exchange(ref m_array[idx], null);
 
-                        m_array[idx] = null;
-                        return obj;
-                    }
-                    else
+                    // Check for race which can occur while TrySteal() & LocalPop() are both taking last item
+                    if (m_headIndex > tail)
                     {
-                        // Interaction with takes: 0 or 1 elements left.
                         bool lockTaken = false;
                         try
                         {
+                            // Must take m_foreignLock to ensure TrySteal() is not repairing m_headIndex
                             m_foreignLock.Enter(ref lockTaken);
 
-                            if (m_headIndex <= tail)
-                            {
-                                // Element still available. Take it.
-                                int idx = tail & m_mask;
-                                IThreadPoolWorkItem obj = Volatile.Read(ref m_array[idx]);
-
-                                // Check for nulls in the array.
-                                if (obj == null) continue;
-
-                                m_array[idx] = null;
-                                return obj;
-                            }
-                            else
+                            // If the queue underflow was not fixed in TrySteal() fix it here
+                            if (m_headIndex > m_tailIndex)
                             {
-                                // If we encountered a race condition and element was stolen, restore the tail.
                                 m_tailIndex = tail + 1;
-                                return null;
                             }
+                            Debug.Assert(m_headIndex <= m_tailIndex);
                         }
                         finally
                         {
@@ -322,6 +303,11 @@ namespace System.Threading
                                 m_foreignLock.Exit(useMemoryBarrier: false);
                         }
                     }
+
+                    // Check for nulls in the array.
+                    if (obj == null) continue;
+
+                    return obj;
                 }
             }
 
@@ -341,24 +327,27 @@ namespace System.Threading
                             {
                                 // Increment head, and ensure read of tail doesn't move before it (fence).
                                 int head = m_headIndex;
-                                Interlocked.Exchange(ref m_headIndex, head + 1);
 
-                                if (head < m_tailIndex)
+                                if(head < m_tailIndex)
                                 {
                                     int idx = head & m_mask;
-                                    IThreadPoolWorkItem obj = Volatile.Read(ref m_array[idx]);
+
+                                    m_headIndex = head + 1;
+
+                                    // We use Interlocked.Exchange to allow LocalPop() to claim from m_array w/o locks
+                                    IThreadPoolWorkItem obj = Interlocked.Exchange(ref m_array[idx], null);
+
+                                    // Check for race which can occur while TrySteal() & LocalPop() are both taking last item
+                                    if (head >= m_tailIndex)
+                                    {
+                                        m_headIndex = head;
+                                    }
 
                                     // Check for nulls in the array.
                                     if (obj == null) continue;
 
-                                    m_array[idx] = null;
                                     return obj;
                                 }
-                                else
-                                {
-                                    // Failed, restore head.
-                                    m_headIndex = head;
-                                }
                             }
                         }
                         finally
-- 
2.7.4

